<?php

?>
<center><h1>Restaurante</h1></center>
<h2>
    <?php 

	if ($data->id != null){
        echo "Reservación con ID: ". $data->id;
    }else{
        echo "Registrar nueva reservación";
    } 
    ?>
</h2>


<body style="background-color:pink;"></body>

<form id="frm-Reservacion" action="?controller=Reservacion&accion=add" method="post">
    <input type="hidden" name="id" value="<?php echo $data->id; ?>" />
    
    <div class="form-group">
        <label>Nombre del Cliente</label>
        <input type="text" name="nombreCliente" value="<?php echo $data->nombreCliente; ?>"  />
    </div>
    
    <div class="form-group">
        <label>Personas</label>
        <input type="text" name="personas" value="<?php echo $data->personas; ?>"  />
    </div>

    <div class="form-group">
        <label>Teléfono</label>
        <input type="text" name="telefono" value="<?php echo $data->telefono; ?>"  />
    </div>

    <div class="form-group">
        <label>Día</label>
        <input type="date" name="dia" min="2018-03-25" value="<?php echo $data->dia; ?>"  />
    </div>

    <div class="form-group">
        <label>Hora</label>
        <input type="time" name="hora" value="<?php echo $data->hora; ?>"  />
    </div>

    <div class="form-group">
        <label>Observaciones</label>
        <input type="text" name="observaciones" value="<?php echo $data->observaciones; ?>"  />
    </div>

    

    
    <hr />
    
    <div>
        <button>Guardar</button>
    </div>
</form>
