<?php
require_once 'model/database.php';
require_once "controller/reservacion.controller.php";

$controller = new ReservacionController();


if(!isset($_REQUEST['controller']))
{      	
    $controller->Index();    
}else{
    $control = ($_REQUEST['controller']);
    $accion = isset($_REQUEST['accion']) ? $_REQUEST['accion'] : 'Index';

    call_user_func(array($controller, $accion));    
}

