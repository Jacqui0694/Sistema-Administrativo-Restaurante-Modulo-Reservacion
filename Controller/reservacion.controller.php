<?php

require_once 'model/Reservacion.php';

class ReservacionController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Reservacion();
    }    
    public function Index(){
        require_once 'view/Reservacion.php';
       
    }    
    public function Crud(){
        $data = new Reservacion();
        
        if(isset($_REQUEST['id'])){
            $data = $this->model->getByID($_REQUEST['id']);
        }       
        require_once 'view/Reservacion-Editar.php';          
    }  
    public function add(){
        $data = new Reservacion();
        
        $data->id = $_REQUEST['id'];
        $data->nombreCliente = $_REQUEST['nombreCliente'];
        $data->personas = $_REQUEST['personas'];
        $data->telefono = $_REQUEST['telefono'];
        $data->dia = $_REQUEST['dia'];
        $data->hora = $_REQUEST['hora'];
        $data->observaciones = $_REQUEST['observaciones'];

        $data->id > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: IndexReservacion.php');
    }
    public function Del(){
        $this->model->Del($_REQUEST['id']);
        header('Location: IndexReservacion.php');
    }
}