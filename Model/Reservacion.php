<?php

include_once("database.php");

class Reservacion{
    
    private $pdo;    
    public $id;
    public $nombreCliente;
    public $personas;
    public $telefono;
    public $dia;
    public $hora;
    public $observaciones;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT id, nombreCliente, personas, telefono, dia, hora, observaciones FROM Reservas");

			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($id)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT id, nombreCliente, personas, telefono, dia, hora, observaciones FROM Reservas WHERE id = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM Reservas WHERE id = ?");			          

			$stm->execute(array($data->id));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE Reservas SET 
						nombreCliente = ?, 
						personas = ?,
						telefono = ?,
						dia = ?,
						hora = ?,
						observaciones = ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
        $data->nombreCliente,
        $data->personas,
        $data->telefono,
        $data->dia,
        $data->hora,
        $data->observaciones,
        $data->id
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Reservacion $data)
	{
		try{
		$sql = "INSERT INTO Reservas(nombreCliente,personas,telefono,dia,hora,observaciones) 
		        VALUES (?, ?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombreCliente,
                    $data->personas,
                    $data->telefono,
                    $data->dia, 
                    $data->hora, 
                    $data->observaciones
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}

?>